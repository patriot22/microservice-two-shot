# Wardrobify

Team:

* Elliott - Hats
* James Keyser - Shoes

## Design

## Shoes microservice

I started with the backend and made sure I could create bins through insomnia. Then implemented my models, views and url paths for shoes. Using restfull api method to create, show a list and delete a shoe. 
## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
