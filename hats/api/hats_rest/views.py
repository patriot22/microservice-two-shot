from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationsVO, Hats

class LocationVOEncoder(ModelEncoder):
    model = LocationsVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
        "id",
    ]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "style_name",
        "color",
        "fabric",
        "pic_url",
    ]

    def get_extra_data(self, object):
        return {"location": object.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "pic_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": list(hats.values())},
            encoder=HatListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)


        location_id = content.get("location")


        if location_id is None:

            location = LocationsVO.objects.create(
                closet_name="Default Closet",
                section_number=1,
                shelf_number=1,
                import_href="default_location"
            )
        else:
            try:

                location = LocationsVO.objects.get(id=location_id)
            except LocationsVO.DoesNotExist:

                location = LocationsVO.objects.create(
                    closet_name="Default Closet",
                    section_number=1,
                    shelf_number=1,
                    import_href=f"default_location_{location_id}"
                )


        hat = Hats.objects.create(
            style_name=content.get("style_name"),
            fabric=content.get("fabric"),
            color=content.get("color"),
            pic_url=content.get("pic_url"),
            location=location
        )


        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse(
                {"message": "Hat not found"},
                status=404
            )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationsVO.objects.get(location=content["location"])
                content["location"] = location
        except LocationsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
