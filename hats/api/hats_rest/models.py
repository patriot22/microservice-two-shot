from django.db import models


class LocationsVO(models.Model):
    closet_name = models.TextField()
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.TextField(unique=True)

    def __str__(self):
        return f'{self.closet_name}'



class Hats(models.Model):
    style_name = models.CharField(max_length=200, null=True)
    fabric = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    pic_url = models.URLField(max_length=200, null=True)

    location = models.ForeignKey(
        LocationsVO,
        related_name="location",
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return f'{self.style_name}'
