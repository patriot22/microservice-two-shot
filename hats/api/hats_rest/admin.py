from django.contrib import admin
from django.conf import settings
from .models import LocationsVO, Hats


@admin.register(LocationsVO)
class LocationsVOAdmin(admin.ModelAdmin):
    list_display = ["closet_name", "section_number", "shelf_number", "import_href"]


@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    list_display = ["style_name", "fabric", "color", "pic_url"]
