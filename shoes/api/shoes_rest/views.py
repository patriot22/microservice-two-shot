from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import BinVO, Shoe
from common.json import ModelEncoder
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name"
    ]


class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "manufacturer"
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
     "id",
     "model_name",
     "color",
     "picture_url",
     "manufacturer",
     "bin"
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoesListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400
            )

        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoesDetailEncoder,
            safe = False
        )

@require_http_methods(["GET","DELETE"])
def show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

    else:
        try:
            shoes = Shoe.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                {"deleted": "true"},
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"}
            )
