import { Link } from 'react-router-dom';
import React, {useEffect, useState} from "react";
import ShowShoe from './ShowShoe';

function ShoesList(props) {
    const [shoes, setShoes] = useState([]);

    const getShoes = async function() {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);
        const shoesObj = await response.json();
        const shoes = shoesObj.shoes;
        setShoes(shoes);
    }

    useEffect(() => {
        getShoes();
    }, [])

    const deleteShoe = async function(id, name) {
        const url = 'http://localhost:8080/api/shoes/${id}/';
        const res = await fetch(url, {method: 'delete'})
        if (res.ok) {
            const newShoesList = shoes.filter(shoe => shoe.id !== id);
            setShoes(newShoesList);
        }
    }

    return (
        <div>
            <h1>Shoes</h1>
            <Link to="/shoes/create/">Track a Shoe!</Link>
            <ul>
                <div className="row">
                    {shoes.map(shoe => <ShowShoe key={shoe.id} shoe={shoe} deleteShoe={deleteShoe}/>)}
                </div>
            </ul>
        </div>
    );

}


export default ShoesList;
