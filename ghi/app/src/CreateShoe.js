import React, { useEffect, useState } from 'react';

function CreateShoe() {
    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const fetchData = async () => {
        const binsUrl = "http://localhost:8100/api/bins/";
        const binsRes = await fetch(binsUrl);
        if (binsRes.ok) {
            const binsData = await binsRes.json();
            setBins(binsData.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefualt();
        const data = {};
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.pictureUrl = picture;
        data.bin = bin;

        const shoeUrl = "http://localhost:8080/api/shoes/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
    }

    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleChangePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    return (
        <div>
            <form onSubmit={handleSubmit} id = "create-form">
                <h1>Shoes go here!!</h1>
                <div>bin<select onChange={handleChangeBin} name="bin" id="bin" className={'form-select'} value={bin}>
                <option>Which Bin</option>
                {bins.map(bin => {
                    return (
                        <option key={bin.import_href} value={bin.import_href}>{bin.closet_name}-{bin.bin_number}</option>
                        )
                })}
                </select>
            </div>
            <div>
                <label htmlFor="modelName">Shoe name</label>
                <input onChange={handleChangeModelName} required placeholder="shoe name" value={modelName} type="text" name="modelName" id="modelName" className="form-control"/>
            </div>
            <div>
                <label htmlFor="modelName">Shoe color</label>
                <input onChange={handleChangeColor} required placeholder="shoe color" value={color} type="text" name="color" id="color" className="form-control"/>
            </div>
            <div>
                <label htmlFor="modelName">Picture URL</label>
                <input onChange={handleChangePicture} required placeholder="picture" value={picture} type="text" name="picture" id="picture" className="form-control"/>
            </div>
            <div>
                <label htmlFor="modelName">Manufacturer</label>
                <input onChange={handleChangeManufacturer} required placeholder="manufacturer" value={manufacturer} type="text" name="manufacturer" id="manufacturer" className="form-control"/>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
    );
}

export default CreateShoe;
