import React from 'react';

function ShowShoe({ shoe, deleteShoe }) {

    return (
        <div className="card w-25 m-2">
            <img src={shoe.pictureUrl} className="card-img-top" />
            <div className="card-body">
                <h3 className="card_title">{shoe.manufacturer.name} - {shoe.model_name}</h3>
                <div>Color: {shoe.color}</div>
                <div>Bin: {shoe.bin.closet_name} - {shoe.bin.bin_number}</div>
                <div className="btn btn-danger w-100 mt-3" onClick={() => deleteShoe(shoe.id, shoe.model_name)} >Delete</div>
            </div>
        </div>
    )
}

export default ShowShoe;
