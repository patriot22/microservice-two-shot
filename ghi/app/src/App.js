import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import CreateShoe from './CreateShoe';
import Hatform from './hatform';
import HatList from './hatlist';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/hats/new/" element={<Hatform />} />
          <Route path="shoes/" element={<ShoesList/>} />
          <Route path="shoes/create/" element={<CreateShoe/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
