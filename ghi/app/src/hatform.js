import React, {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';

function Hatform() {

    const [style_name, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [pic_url, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);
    const navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await fetch('http://localhost:8100/api/locations');
            if (response.ok) {
              const data = await response.json();
              setLocations(data.locations);
            }
          } catch (error) {
            console.log('Error fetching locations:', error);
          }
        }

        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
          fabric,
          style_name,
          color,
          pic_url,
          location,
        };

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
          console.log(newHat)
          setFabric('');
          setStyle('');
          setColor('');
          setPictureUrl('');
          setLocation('');
        }
        navigate('/hats')
      }

      function handleFabricChange(event) {
        const { value } = event.target;
        setFabric(value);
      }

      function handleStyleChange(event) {
        const { value } = event.target;
        setStyle(value);
      }

      function handleColorChange(event) {
        const { value } = event.target;
        setColor(value);
      }

      function handlePictureUrlChange(event) {
        const { value } = event.target;
        setPictureUrl(value);
      }

      function handleLocationChange(event) {
        const { value } = event.target;
        setLocation(value);
      }


      return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Add Your SICK Hat!</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className='form-floating mb-3'>
                <input value={style_name} onChange={handleStyleChange} placeholder='Style' required type='text' name='style' id='style' className='form-control'/>
                <label htmlFor='style'>Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="fabric" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={pic_url} onChange={handlePictureUrlChange} placeholder="https://www.example.com" required type="url" name="pic_url" id="pic_url" className="form-control"/>
                <label htmlFor="pic_url">Picture URL</label>
              </div>
              <div className='mb-3'>
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option>Choose a location</option>
                  {locations.map(location => {
                    return (
                     <option key={location.id} value={location.id}>
                        {location.closet_name}
                      </option>
                    );
                  })};
              </select>
              </div>
              <button className='btn btn-primary'>Add to Collection</button>
              </form>
          </div>
        </div>
        </div>
        </>
    );
}

export default Hatform
