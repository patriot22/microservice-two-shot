import React, { useEffect, useState } from "react";
// import { Link } from "react-router-dom";
import './index.css'


function HatList() {
    const [hats, setHats] = useState([]);

    const getData = async () => {
        const url = 'http://localhost:8090/api/hats';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    };

    const deleteHat = async (id) => {
        const response = await fetch(`http://localhost:8090/api/hats/${id}`, {
            method: 'delete',
        });
        if (response.ok) {
            getData();
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="display-4 text-center mb-4 font-italic">All of my STUPID hats</h1>

                    <table className="table table-striped">
                        <thead className="thead-dark">
                            <tr>
                                <th>Fabric</th>
                                <th>Style</th>
                                <th>Color</th>
                                <th>Picture</th>
                                <th>Location</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {hats.map((hat) => (
                                <tr key={hat.id}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style_name}</td>
                                    <td>{hat.color}</td>
                                    <td>
                                        <img
                                            src={hat.pic_url}
                                            alt=""
                                            width="100px"
                                            height="100px"
                                        />
                                    </td>
                                    <td>{hat.location_id}</td>
                                    <td>
                                        <button
                                            onClick={() => deleteHat(hat.id)}
                                            type="button"
                                            className="btn btn-danger"
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default HatList;
